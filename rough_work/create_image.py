import cv2
import numpy as np


def function1():
    # color_image = np.zeros((512, 512, 3))
    # bw_image = np.zeros((512, 512))

    # read file
    file1 = open('/home/amit/Documents/small_set.csv', 'r')

    # line = file1.readline()
    # row = file1.readline().split(',')
    pixel = file1.readline().split(',')[1:]
    print(pixel)
    # print(line)
    pixel = np.asarray(pixel, dtype=np.uint8).reshape((28, 28, 1))
    print(f"shape : {pixel.shape}, type : {type(pixel)}")
    print(pixel)


    file1.close()

    # save the images in images directory
    # cv2.imwrite("image_A.png", pixel)

    # cv2.imshow("Color Image", color_image)
    cv2.imshow("Image", pixel)
    cv2.waitKey(0)
    cv2.destroyAllWindows()




function1()