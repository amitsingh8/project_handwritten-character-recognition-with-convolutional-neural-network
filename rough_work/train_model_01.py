# import required packages
import tensorflow as tf
import matplotlib.pyplot as plt
import numpy as np
from tensorflow.keras.preprocessing import image


class myCallback(tf.keras.callbacks.Callback):
    def on_epoch_end(self, epoch, logs={}):
        if logs.get('accuracy') > 0.99:
            print("\nReached 99% accuracy so training is stop !")
            self.model.stop_training = True


model = tf.keras.models.Sequential([
    tf.keras.layers.Conv2D(32, (3, 3), activation='relu', input_shape=(28, 28, 1)),
    tf.keras.layers.MaxPooling2D(2,2),
    tf.keras.layers.Conv2D(64, (3, 3), activation='relu'),
    tf.keras.layers.MaxPooling2D(2, 2),
    tf.keras.layers.Flatten(),
    tf.keras.layers.Dense(128, activation='relu'),
    tf.keras.layers.Dense(26, activation='softmax')
])

# print model of summary
print(model.summary())


from tensorflow.keras.optimizers import RMSprop

model.compile(optimizer=RMSprop(lr=0.001),
              loss='categorical_crossentropy',
              metrics=['accuracy'])
print("complie done")

callback = myCallback()
train_dir = '/home/amit/Sunbeam/Project/project_handwritten-character-recognition-with-convolutional-neural-network/Codes/train'
test_dir = '/home/amit/Sunbeam/Project/project_handwritten-character-recognition-with-convolutional-neural-network/Codes/test'

train_datagen = tf.keras.preprocessing.image.ImageDataGenerator(rescale=1.0/255.)
test_datagen = tf.keras.preprocessing.image.ImageDataGenerator(rescale=1.0/255.)

train_generator = train_datagen.flow_from_directory(
        train_dir,
        target_size=(28, 28),
        color_mode='grayscale',
        batch_size=7351,
        # use categorical crossenotropy loss, so need of categorical label
        class_mode='categorical'
    )

test_generator = test_datagen.flow_from_directory(
        test_dir,
        target_size=(28, 28),
        # adjust number of image in test directory to make its steps 38 in model.fit
        batch_size=2450,
        color_mode='grayscale',
        # use categorical crossenotropy loss, so need of categorical label
        class_mode='categorical'
    )


# fit the model
history = model.fit(
    train_generator,
    validation_data=test_generator,
    validation_steps=38,
    steps_per_epoch=38,
    epochs=25,
    callbacks=callback,
    verbose=1
    )

# save the model
model.save('model_02')


# training and test accuracy VS epochs
def visualiztion():
    acc = history.history['accuracy']
    val_acc = history.history['val_accuracy']
    loss = history.history['loss']
    val_loss = history.history['val_loss']

    epochs = range(len(acc))

    plt.plot(epochs, acc, 'r', label='Training accuracy')
    plt.plot(epochs, val_acc, 'b', label='Validation accuracy')
    plt.title('Training and validation accuracy')
    plt.legend(loc=0)
    plt.figure()

    plt.show()


visualiztion()


# predict images
def predict():
    img = tf.keras.preprocessing.image.load_img('/home/amit/Downloads/3386844.jpg', grayscale=True, target_size=(28, 28, 1))
    x = tf.keras.preprocessing.image.img_to_array(img)
    x = np.expand_dims(x, axis=0)
    # print(model.predict(x), model.predict(x)[0][0])
    pridiction = model.predict(x)
    word_dict = {0: 'A', 1: 'B', 2: 'C', 3: 'D', 4: 'E', 5: 'F', 6: 'G', 7: 'H', 8: 'I', 9: 'J', 10: 'K', 11: 'L',
                 12: 'M', 13: 'N',
                 14: 'O', 15: 'P', 16: 'Q', 17: 'R', 18: 'S', 19: 'T', 20: 'U', 21: 'V', 22: 'W', 23: 'X', 24: 'Y',
                 25: 'Z'}
    l1 = list(pridiction[0])
    for i in l1:
        if i == 1:
            print(word_dict.get(l1.index(i)))

predict()
