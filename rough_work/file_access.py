import  math
import os


def line_by_line():
    file1 = open('/home/amit/Documents/small_set.csv', 'r')
    # lines = file1.readline()
    count = 0

    # print(lines)

    while True:
        count += 1

        line = file1.readline()

        if not line:
            break
        # print(f"count {count}    line -  {type(line.strip())}")
        row = line.split(',')
        # print(type(row))
        print(row[0])
        pixel = row[1:]
        print(pixel)
        print(math.sqrt(len(pixel)))

    file1.close()


# line_by_line()


def create_directory():
    # read file
    file1 = open('/home/amit/Documents/small_set.csv', 'r')

    directories = []
    parent_dir = os.getcwd()
    count = 0


    # getting lines
    while True:
        line = file1.readline()
        if not line:
            break
        row = line.split(',')
        directory = str(row[0])
        # print(type(directory))
        path = os.path.join(parent_dir, directory)
        count += 1
        directories = os.listdir(parent_dir)
        if directory in directories:
            f = open(f'{directory}/image_{count}.txt', 'x')
            print(f"{count} - not created directory only file add")
        else:
            try:
                os.mkdir(path)
            except OSError as error:
                print(error)
            f = open(f'{path}/image_{count}.txt', 'x')
            print(f"{count} - created directory and file add")


        # pixel = row[1:]
        # print(pixel)
        # print(math.sqrt(len(pixel)))

create_directory()
