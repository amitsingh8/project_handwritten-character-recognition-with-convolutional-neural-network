import os

print(os.getcwd())

directory = 'train'

parent_dir = os.getcwd()

path = os.path.join(os.getcwd(), 'train')

print(path)

# os.mkdir(path)

# print(f'Directory {directory} is created')

# Checking if Python Directory Exists

print('Path exists : ', os.path.exists(path))

# directories = []

# directories = os.listdir(parent_dir)

# for i in range(10):
#     if directory in directories:
#         print("that is here :)")
#         f = open(f'{path}/myfile{i}.txt', 'x')
#
#
#
# print(type(directories), directories)


def value_access():
    row = [0, 2, 6, 5, 8, 9, 7]
    word_dict = {0: 'A', 1: 'B', 2: 'C', 3: 'D', 4: 'E', 5: 'F', 6: 'G', 7: 'H', 8: 'I', 9: 'J', 10: 'K', 11: 'L',
                 12: 'M', 13: 'N',
                 14: 'O', 15: 'P', 16: 'Q', 17: 'R', 18: 'S', 19: 'T', 20: 'U', 21: 'V', 22: 'W', 23: 'X', 24: 'Y',
                 25: 'Z'}

    x = word_dict.get(row[2])
    print(f"type of value : {type(x)}, and value is {x}")

value_access()