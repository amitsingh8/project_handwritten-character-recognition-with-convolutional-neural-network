import pandas as pd
import numpy as np


def save_file():
    x = pd.read_csv('/home/amit/Documents/small_set.csv', header=None)

    print(x.head(3))

    # just to pass in train and test split
    y = list(map(lambda i: i, range(len(x))))
    print(f"nnumber of row in y: {len(y)} and its type is {type(y)}")
    y = np.asarray(y)
    print(y)
    print(f"type : {type(y)}, shape : {y.shape}")

    print(f"number of row in x : {len(x)} and its type is {type(x)}")
    from sklearn.model_selection import train_test_split

    x_train, x_test, y_train, y_test = train_test_split(x, y, test_size=0.25, random_state=42)

    print(x_train)
    # save this data frame as CSV
    x_train.to_csv('train.csv', index=False)

    print('-' * 60)

    print(x_test)
    # save this data frame as CSV
    x_test.to_csv('test.csv', index=False)


# save_file()

def access_train_test():
    # train = pd.read_csv('train.csv')
    # test = pd.read_csv('test.csv')
    # test = pd.read_csv('/home/amit/Sunbeam/Project/project_handwritten-character-recognition-with-convolutional-neural-network/Codes/test.csv')
    train = pd.read_csv('/home/amit/Sunbeam/Project/project_handwritten-character-recognition-with-convolutional-neural-network/Codes/train.csv')

    print(train.tail(10))
    print(f"shape od train : {train.shape}")
    print('-' * 60)
    # print(test.tail(10))
    # print(f"shape od test : {test.shape}")


access_train_test()